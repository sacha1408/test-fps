﻿using UnityEngine;

public class Target : MonoBehaviour
{
    public float maxHealth = 50f;
    public float health;
    public Canvas hpBar;
    public RectTransform bar;
    public ParticleSystem dedExpl;
    private GameObject player;
    public float diffMult = 1f;

    void Start() {
        player = GameObject.FindGameObjectWithTag("Player");

        switch(PlayerPrefs.GetInt("Difficulty", 1)){
            case 0:
            diffMult = 0.75f;
            break;

            case 1:
            diffMult = 1f;
            break;

            case 2:
            diffMult = 1.3f;
            break;
        }

        maxHealth *= diffMult;
        health = maxHealth;
    }

    void Update()
    {
        if (health <= 0f)
        {
            dedExpl.Play();
            Invoke("die", 0.2f);
        }

        bar.offsetMin = new Vector2((2 - (2*(health/maxHealth))), bar.offsetMax.y);
        hpBar.transform.LookAt(player.transform);
    }

    void die(){
        Destroy(gameObject);
    }
}
