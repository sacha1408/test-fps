﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;
using System.Globalization;


public class SettingsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    public TMP_Dropdown resDropdown;
    public TMP_Dropdown qualDropdown;
    public TMP_InputField SensiInput;
    public Slider fovSlide;
    public Slider volSlide;
    public Toggle FSToggle;
    public TMP_Dropdown DiffDropdown;
    Resolution[] resolutions;

    void Start(){
        //affichage résolutions
        resolutions = Screen.resolutions;

        resDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currResIndex = 0;
        for(int i = 0; i < resolutions.Length; i++){
            string option = resolutions[i].ToString();
            options.Add(option);

            if(resolutions[i].width == Screen.width && resolutions[i].height == Screen.height){
                currResIndex = i;
            }
        }
        Debug.Log(Screen.currentResolution);
        resDropdown.AddOptions(options);

        resDropdown.value = currResIndex;
        resDropdown.RefreshShownValue();

        //affichage sensi
        SensiInput.text = PlayerPrefs.GetFloat("sensi", 1f).ToString(CultureInfo.InvariantCulture);

        //affichage FS
        FSToggle.isOn = bool.Parse(PlayerPrefs.GetString("FS", "true"));

        //affichage qual
        qualDropdown.value = PlayerPrefs.GetInt("quality", 0);

        //affichage fov
        fovSlide.value = PlayerPrefs.GetFloat("FoV", 80);

        //affichage volume
        audioMixer.SetFloat("volume", PlayerPrefs.GetFloat("volume"));
        audioMixer.GetFloat("volume", out float vol);
        volSlide.value = vol;

        //affichage difficulty
        DiffDropdown.value = PlayerPrefs.GetInt("Difficulty", 1);
    }

    public void SetVolume(float volume){
        audioMixer.SetFloat("volume", volume);
        PlayerPrefs.SetFloat("volume", volume);
    }

    public void setQuality(int qualityindex){
        QualitySettings.SetQualityLevel(qualityindex);
        PlayerPrefs.SetInt("quality", qualityindex);
    }

    public void setFullscreen(bool isFullscreen){
        Screen.fullScreen = isFullscreen;
        PlayerPrefs.SetString("FS", isFullscreen.ToString());
    }

    public void setRes(int resIndex){
        Resolution res = resolutions[resIndex];
        Screen.SetResolution(res.width, res.height, Screen.fullScreen);
    }

    public void setSens(string sensi){
        float sensit = float.Parse(sensi, CultureInfo.InvariantCulture);
        PlayerPrefs.SetFloat("sensi", sensit);
    }

    public void setFov(float fov){
        PlayerPrefs.SetFloat("FoV", fov);
    }

    public void setDifficulty(int diff){
        PlayerPrefs.SetInt("Difficulty", diff);
    }
}
