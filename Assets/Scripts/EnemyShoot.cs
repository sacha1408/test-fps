﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    public int damage;
    public float range;
    public float force;
    public bool shootable = false;
    public AudioSource shootSound;
    public ParticleSystem shootParticle;
    public float fireRate; 
    public GameObject bullet;
    RaycastHit hit;
    float currRate;
    float actSkill;
    GameObject player;
    float diffMult;

    // Start is called before the first frame update
    void Start()
    {
        currRate = 0f;
        player = GameObject.FindGameObjectWithTag("Player");
        switch(PlayerPrefs.GetInt("Difficulty", 1)){
            case 0:
            diffMult = 0.75f;
            break;

            case 1:
            diffMult = 1f;
            break;

            case 2:
            diffMult = 1.3f;
            break;
        }
        Debug.Log(diffMult);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //viser le joueur
        transform.LookAt(player.transform);

       //appel à shoot() si detection 
       if(shootable && Time.time >= currRate){
           shoot();
       }
    }

    void shoot()
    {
        shootSound.Play();
        shootParticle.Play();

        //instancier la balle, assigner les dmg de l'arme et la pousser
        GameObject bulletClone = Instantiate(bullet, transform.position, transform.rotation);
        bulletClone.GetComponent<Bullet>().damage = Mathf.RoundToInt(damage*diffMult);
        bulletClone.GetComponent<Rigidbody>().velocity = transform.forward * (force * diffMult);

        currRate = Time.time + 1f/((fireRate*diffMult)/60);
    }
}
