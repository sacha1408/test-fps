﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMove : MonoBehaviour
{
    GameObject player;
    NavMeshAgent agent;
    SphereCollider visionZone;
    EnemyShoot shoot;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        agent = GetComponent<NavMeshAgent>();
        visionZone = GetComponent<SphereCollider>();
        shoot = GetComponentInChildren<EnemyShoot>();
        agent.stoppingDistance = shoot.range / 2;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        agent.SetDestination(player.transform.position);
    }

    private void OnTriggerStay(Collider other) {
        if(other.gameObject.tag == "Player"){
            shoot.shootable = true;
            transform.LookAt(player.transform);
        }
    }

    private void OnTriggerExit(Collider other) {
         if(other.gameObject.tag == "Player"){
            shoot.shootable = false;
        }
    }
}
