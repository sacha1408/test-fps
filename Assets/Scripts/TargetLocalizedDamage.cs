﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetLocalizedDamage : MonoBehaviour
{
    public Target mainTarget;
    public float dmgMultiplier;

    public void takeDamage(float amount)
    {
        float totalAmount = amount * dmgMultiplier;
        mainTarget.health -= totalAmount;
    }

    
}
