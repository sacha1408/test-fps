﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float maxHp = 100;

    public float hp;

    public RectTransform bar;

    // Start is called before the first frame update
    void Start()
    {
        hp = maxHp;
    }

    void FixedUpdate()
    {
        bar.offsetMin = new Vector2((200 - (200*(hp/maxHp))), bar.offsetMin.y);

        if(hp <= 0){
            Debug.Log("u ded");
            FindObjectOfType<GameManager>().gameOver();
        }
    }
}
