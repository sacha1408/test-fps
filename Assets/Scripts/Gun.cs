﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public int damage = 10;
    public float force = 30f;
    public float fireRate = 100f;
    public bool auto = false;

    public PlayerMove bouge;
    public Transform player;
    public Camera fpsCam;
    public ParticleSystem muzzleFlash;
    public ParticleSystem bulletTrail;
    public AudioSource shoot;
    public Animator anim;
    public GameObject bullet;
    float waitTime = 0f;
    bool shootable = true;

    void Update()
    {
        if(Time.time >= waitTime)
        {
            //check cadence de tir
            shootable = true;
            anim.SetBool("shooting", false);
        }

        if (auto)
        {
            if (Input.GetButton("Fire1") && !bouge.isSprinting && shootable)
            {
                Shoot();
            }
        }
        else
        {
            if (Input.GetButtonDown("Fire1") && !bouge.isSprinting && shootable)
            {
                Shoot();
            }
        }
    }

    void Shoot()
    {
        //animation
        anim.SetBool("shooting", true);
        shootable = false;
        //init tps cadence de tir
        waitTime = Time.time + 1f/(fireRate/60);
        //particules et sons
        muzzleFlash.Play();
        shoot.Play();

        //tir et hit
        /*
        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, 50f))
        {
            
            Debug.Log(hit.transform.name);

            TargetLocalizedDamage target = hit.transform.GetComponent<TargetLocalizedDamage>();
            if (target != null)
            {
                target.takeDamage(damage);
            }

            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * force);
            }

        }*/

        //instancier la bullet sur le gun
        //add beaucoup de force
        GameObject bulletClone = Instantiate(bullet, transform.position, transform.rotation);
        bulletClone.GetComponent<Bullet>().damage = damage;
        bulletClone.GetComponent<Bullet>().playerFired = true;
        bulletClone.GetComponent<Rigidbody>().velocity = fpsCam.transform.forward * force;
        
    }
}
