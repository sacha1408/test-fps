﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{

    public CharacterController controller;
    public float speed = 12f;
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    public float jumpHeight = 2f;
    private float initialSpd;

    Vector3 velocity;
    public float grav = -9.81f;

    bool isGrounded;
    bool isDoubleJump = true;

    public AudioSource groundSound;
    bool isGroundPlayed = false;
    public AudioSource jump;

    public GameObject weapon;
    public bool isSprinting = false;
    public Animator anim;

    void Start()
    {
        initialSpd = speed;
    }

    // Update is called once per frame
    void Update()
    {
        //check on the ground
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        if (isGrounded && velocity.y < 0)
        {
            isDoubleJump = true;
            velocity.y = -2f;
            if (isGroundPlayed)
            {
                groundSound.Play();
                isGroundPlayed = false;
            }
        }

        //check movement
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        Vector3 move = transform.right * x + transform.forward * z;
        controller.Move(move * speed * Time.deltaTime); //deltatime -> independant du framerate

        //check gravity and free falling
        velocity.y += grav * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        //check for jump and double jump
        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * grav);
            jump.Play();
            isGroundPlayed = true;
        }
        if(!isGrounded && isDoubleJump && Input.GetButtonDown("Jump"))
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * grav);
            isDoubleJump = false;
            jump.Play();
        }

        //check for crouching
        if (Input.GetButtonDown("crouch"))
        {
            transform.localScale = new Vector3(1f, 0.6f, 1f);
            if (isGrounded)
            {
                speed /= 1.5f;
            }
        }
        if (Input.GetButtonUp("crouch"))
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
            speed = initialSpd;
        }

        //check for sprinting
        if (Input.GetButtonDown("sprint"))
        {
            speed *= 1.75f;
            isSprinting = true;
            anim.SetBool("sprinting", true);
        }
        if (Input.GetButtonUp("sprint"))
        {
            speed = initialSpd;
            isSprinting = false;
            anim.SetBool("sprinting", false);
        }

    }
}
