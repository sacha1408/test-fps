﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseLook : MonoBehaviour
{
    public Transform playerBody;
    public Transform gun;

    Camera cam;

    private float yRotation = 0f;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * PlayerPrefs.GetFloat("sensi", 1);
        float mouseY = Input.GetAxis("Mouse Y") * PlayerPrefs.GetFloat("sensi", 1);

        playerBody.Rotate(Vector3.up * mouseX);
        
        yRotation -= mouseY;
        yRotation = Mathf.Clamp(yRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(yRotation, 0f, 0f);
        gun.localRotation = Quaternion.Euler(- yRotation, 0f, 0f);
        
        cam.fieldOfView = PlayerPrefs.GetFloat("FoV", 80);
    }
}
