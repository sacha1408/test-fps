﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public bool playerFired = false;
    public int damage;
    float firedTime;

    // Start is called before the first frame update
    void Start()
    {
        firedTime = Time.time;
    }

    private void FixedUpdate() {
        if(Time.time - firedTime >= 3f){
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Player" && !playerFired){
            Player pl = other.gameObject.GetComponent<Player>();
            if(pl){
                pl.hp -= damage;
            }
        }else{
            TargetLocalizedDamage tld = other.gameObject.GetComponent<TargetLocalizedDamage>();
            if(tld != null){
                tld.takeDamage(damage);
            }
        }
    }
}
