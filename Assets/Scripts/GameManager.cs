﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject gameOverUI;

    public void gameOver(){
        gameOverUI.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        FindObjectOfType<mouseLook>().enabled = false;
        FindObjectOfType<PlayerMove>().enabled = false;
        FindObjectOfType<Gun>().enabled = false;
        Cursor.visible = true;
    }

    public void restart(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
